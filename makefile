PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/share/man/man1

.POSIX: install

all: help

help:
	@echo "please run 'make install' as root"
install:
	cp ./wgh.sh $(BINDIR)/wgh
	cp ./wgh.1 $(MANDIR)
uninstall:
	rm $(BINDIR)/wgh
	rm $(MANDIR)/wgh.1
test:
	shellcheck wgh.sh

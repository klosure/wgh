# wgh

[![License](https://is.gd/GbWFMI)](https://opensource.org/licenses/ISC)

**W**ire**G**uard **H**elper

An interactive command line tool to help you bring wireguard connections up and down.

### Dependencies

- [Wireguard](https://www.wireguard.com/) (*obviously*)
- [sudo](https://www.sudo.ws/) (on linux)
- POSIX [shell](https://pubs.opengroup.org/onlinepubs/009695399/utilities/xcu_chap02.html)
- POSIX [make](https://pubs.opengroup.org/onlinepubs/009695399/utilities/make.html)

### Installation

Simply run:

```bash
$ git clone https://gitlab.com/klosure/wgh.git
$ cd wgh
$ sudo make install

# to uninstall
$ sudo make uninstall
```

### Usage

```bash
$ wgh                             # start wgh in interactive mode
$ wgh -l                          # lock or unlock /etc/resolv.conf
$ wgh -s                          # show the connection status info
$ wgh -h                          # shows this help message
$ wgh -v                          # show the version info
```

### Video Example

[![asciicast](https://asciinema.org/a/UKHpa0qMd9ac0bxzpN1zdetoV.svg)](https://asciinema.org/a/UKHpa0qMd9ac0bxzpN1zdetoV)

### Notes
The `-l` flag exists to address a bug I found on Void Linux where I was leaking DNS requests. NetworkManager can periodically overwrite the value in `/etc/resolv.conf`. The `-l` flag simply prevents that file from being modified while your tunnel is up.

### TODO

- test debian / freebsd / openbsd
- test cleanup
- -u and -d flags (up,down .conf)
- blink1 integration

### License / Disclaimer
This project is licensed under the ISC license. (See [LICENSE.md](LICENSE.md))



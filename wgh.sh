#!/bin/sh
#
# wgh - a wireguard cli helper tool
# author: klosure
# https://gitlab.com/klosure/wgh 
# license: isc


VERSION="0.1"

# GLOBAL VARS
# default to sudo, will be doas on OpenBSD
SUPER_CMD="sudo"
# attempt to lock /etc/resolv.conf?
LOCKING="false"
# if there is a connection, what server are we connected to?
CONNECTED_TO=""


# locks the /etc/resolv.conf file
# necessary for Void Linux to prevent DNS leaks
# NetworkManager wants to reset the DNS server
fun_lock_resolv () {
    if [ "$LOCKING" = "true" ] ; then
	$SUPER_CMD chattr +i /etc/resolv.conf && echo "wgh: /etc/resolv.conf successfully locked"
    fi
}


# unlocks /etc/resolv.conf
fun_unlock_resolv () {
    if [ "$LOCKING" = "true" ] ; then 
       $SUPER_CMD chattr -i /etc/resolv.conf && echo "wgh: /etc/resolv.conf successfully unlocked"
    fi
}


# start wireguard connection
# $1 = server to connect to
fun_start_wg () {
    wg-quick up "$1"
    fun_lock_resolv
}


# stop wireguard connection
fun_stop_wg () {
    fun_unlock_resolv
    # We want this behavior for building a command line
    # leading space in ' se' was causing issue
    # shellcheck disable=SC2086
    wg-quick down $CONNECTED_TO
}


# is there already a wireguard connection up?
fun_is_wg_up () {
    L_CON_FOUND="$($SUPER_CMD wg show | grep peer | cut -f1 -d":")"
    if [ "$L_CON_FOUND" = "peer" ] ; then
	CONNECTED_TO="$($SUPER_CMD wg show | grep interface | cut -f2 -d":")" # set the server we are currently connected to
	unset L_CON_FOUND
	return 0 # connection up
    fi
    unset L_CON_FOUND
    return 1 # connection down
}


# list out all the possible configs in /etc/wireguard
fun_list_configs () {
    for FILE in $($SUPER_CMD find /etc/wireguard -type f -name "*.conf") ; do
	basename "$FILE"
    done
}


# show our tunnel info
fun_show_status () {
    L_CON_FOUND="$($SUPER_CMD wg show | grep peer | cut -f1 -d":")"
    if [ "$L_CON_FOUND" = "peer" ] ; then
	$SUPER_CMD wg show
    else
	echo "wgh: not currently connected."
    fi
    unset L_CON_FOUND
}


# check what OS is we are running under
fun_check_os () {
    L_OS="$(uname)"
    if [ "$L_OS" = "OpenBSD" ] ; then
	SUPER_CMD="doas"
    fi
}


# run when ctrl-c is pressed
fun_cleanup () {
    echo ""
    echo "Caught sigint, cleaning up..."
    # TODO
    unset VERSION SUPER_CMD LOCKING CONNECTED_TO
    exit 0
}


# prints out the version of boil
fun_print_ver () {
    echo "version: $VERSION"
}


# print general info
fun_print_info () {
    echo "wgh - a wireguard cli helper"
    fun_print_ver
    echo "author: klosure"
    echo "site: https://gitlab.com/klosure/wgh"
    echo "license: isc"
}


# prints out the help menu
fun_print_help () {
    fun_print_info
    echo ""
    echo "--- usage --------------------------------------------------"
    echo "wgh                          # start wgh in interactive mode"
    echo "wgh -l                       # lock or unlock /etc/resolv.conf"
    echo "wgh -s                       # show the connection status info"
    echo "wgh -h                       # shows this help message"
    echo "wgh -v                       # show the version info"
}


# prints out a general error message
fun_error () {
    echo "Uh Oh, something borked!"
    echo "$1"
    fun_print_help
    exit 1
}


fun_interactive () {
    fun_print_info
    echo ""
    if fun_is_wg_up ; then
	echo "wgh: Disconnecting..."
	fun_stop_wg
	exit 0
    fi
    echo "Starting interactive mode..."
    L_PICK=""
    fun_list_configs
    # yes, we really do want only read, and not read -r. We DONT want backslashes.
    printf "> What server would you like to connect to?: "
    # shellcheck disable=SC2162
    read L_PICK
    L_PICK_BASE="${L_PICK%%.*}" # strip off extension - file.conf to file
    echo "wgh: Connecting to server: $L_PICK_BASE..."
    fun_start_wg "$L_PICK_BASE" 
    unset L_PICK L_PICK_BASE
}


# make sure that our core dependencies are installed
fun_check_deps () {
    if ! [ -x "$(command -v wg-quick)" ]; then
	fun_error "error: wgh needs wireguard, please install it."
    elif ! [ -x "$(command -v $SUPER_CMD)" ] ; then
	# this will trigger if you are on a linux system without sudo (OpenBSD should always have doas installed)
	fun_error "error: wgh needs $SUPER_CMD, please install it"
    fi
}


# --- ENTRY POINT ---

trap 'fun_cleanup' INT # catch a ctrl-c
fun_check_os
fun_check_deps

# Did the user give any flags to us?
if [ "$#" = "0" ] ; then
    fun_interactive
else
    # parse the user input
    while getopts ':slvh' OPT ; do
	case "$OPT" in
	    v)      fun_print_ver && exit 0;;
	    h)      fun_print_help && exit 0;;
	    s)      fun_show_status && exit 0;;
	    l)      LOCKING="true" && fun_interactive;;
	    [?])    fun_error;;
	esac
    done
fi

unset VERSION SUPER_CMD LOCKING CONNECTED_TO

exit 0
